<?php

/**
 *	@module			pw_generator
 *	@version		see info.php of this module
 *	@author			cms-lab
 *	@copyright		2016-2023 cms-lab
 *	@license		copyright, all rights reserved
 *	@license_terms	please see info.php of this module 
 *	@platform		see info.php of this module
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

//Modul Description
$module_description = 'Erzeuge einfach sichere Passworte.';

//Variables for the backend
$MOD_PW_GENERATOR = [
	'PASSWORD'	=> 'Dein generiertes Passwort:',
	'CHOOSE'	=> 'Wähle Vorgaben',	
	'PW_RESULT_NAME'	=> 'Passwort: ',
	'ALPHA_UPPER' => 'Großbuchstaben (A-Z): ',
	'ALPHA_LOWER' => 'Kleinbuchstaben (a-z): ', 
	'INCLUDE_NUMBER' => 'Zahlen (0-9): ', 
	'INCLUDE_SYMBOL' => 'Sonderzeichen: ', 
	'PW_LENGTH' =>'Passwort Länge:',
	'GENERATE_PW' =>'Passwort erzeugen',
	'YES' =>'Ja',
	'NO' =>'Nein'
];
