<?php

/**
 *	@module			pw_generator
 *	@version		see info.php of this module
 *	@author			cms-lab
 *	@copyright		2016-2023 cms-lab
 *	@license		copyright, all rights reserved
 *	@license_terms	please see info.php of this module 
 *	@platform		see info.php of this module
 */


// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {	
	include LEPTON_PATH.SEC_FILE;
} else {
	$oneback = "../";
	$root = $oneback;
	$level = 1;
	while (($level < 10) && (!file_exists($root.SEC_FILE))) {
		$root .= $oneback;
		$level += 1;
	}
	if (file_exists($root.SEC_FILE)) { 
		include $root.SEC_FILE;   
	} else {
		trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
	}
}
// end include secure.php

$debug = true;

if (true === $debug)
{
    ini_set('display_errors', 1);
    error_reporting(E_ALL | E_STRICT);
}

// get instance of functions file
$oPWG = pw_generator::getInstance();

if(isset ($_GET['tool']) && (empty($_POST)) ) {
	$oPWG->display('show');	
} elseif(isset ($_POST['job']) && ($_POST['job'] == 'generate') ) {
	$oPWG->display('generate'); 
} elseif(isset ($_POST['show_info']) && ($_POST['show_info']== 'show') ) {
	$oPWG->show_info();
} else {
	die('something went wrong');
}
